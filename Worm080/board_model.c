// A simple variant of the game Snake
//
// Used for teaching in classes
//
// Author:
// Franz Regensburger
// Ingolstadt University of Applied Sciences
// (C) 2011
//
// The board model
#include <stdlib.h>
#include <curses.h>
#include "worm.h"
#include "messages.h"
#include "board_model.h"
// Place an item onto the curses display.

enum ResCodes initializeBoard(struct board* aboard){
  //max index of a row
  aboard->last_row = LINES - ROWS_RESERVED - 1;
  // max index of column
  aboard->last_col = COLS - 1;

  if(aboard->last_col < MIN_NUMBER_OF_COLS - 1 ||
     aboard->last_row < MIN_NUMBER_OF_ROWS - 1) {
    char buf[100];
    sprintf(buf, "Das Fenster is zu klein: wir brauchen %dx%d",
            MIN_NUMBER_OF_COLS, MIN_NUMBER_OF_ROWS + ROWS_RESERVED);
    showDialog(buf,"Bitte eine Taste drücken");
    return RES_FAILED;
  }

  // alloc array of rows
  aboard->cells = (enum BoardCodes**) malloc((LINES - ROWS_RESERVED) * sizeof(enum BoardCodes*));
  if(aboard->cells == NULL) {
    showDialog("Abbruch: Zu wenig Speicher", "Bitte eine Taste drücken");
    exit(RES_FAILED);
  }
  int y;
  for(y=0; y < (LINES - ROWS_RESERVED); y++) {
    // Allocate array of cols for each y
    aboard->cells[y] = (enum BoardCodes*) malloc(COLS * sizeof(enum BoardCodes));
    if(aboard->cells[y] == NULL) {
      showDialog("Abbruch: Zu wenig Speicher", "Bitte eine Taste drücken");
      exit(RES_FAILED);
    }
  }
  return RES_OK;
}

void cleanupBoard(struct board* aboard) {
  int y;
  for(y=0; y < (LINES - ROWS_RESERVED); y++) {
    free(aboard->cells[y]);
  }
  free(aboard->cells);
}

void placeItem(struct board* aboard,
               int y, int x,
               enum BoardCodes board_code,
               chtype symbol,
               enum ColorPairs color_pair) {
  move(y, x);
  attron(COLOR_PAIR(color_pair));
  addch(symbol);
  attroff(COLOR_PAIR(color_pair));
  aboard -> cells[y][x] = board_code;
}

enum ResCodes initializeLevel(struct board* aboard) {
  int x,y; //define local variables for loops etc
  for(y=0; y <= aboard->last_row; y++) {
      for(x=0; x <= aboard->last_col; x++) {
        placeItem(aboard,y,x,BC_FREE_CELL,SYMBOL_FREE_CELL,COLP_FREE_CELL);
      }
  }

  // draw a line in order to seperate the message area
  // NOTE: we cannot use function placeItem() since the message area
  // is outside the of board!
  y = aboard->last_row +1;
  for(x=0; x <= aboard->last_col; x++) {
    move(y,x);
    attron(COLOR_PAIR(COLP_BARRIER));
    addch(SYMBOL_BARRIER);
    attroff(COLOR_PAIR(COLP_BARRIER));
  }

  // Barriers: use a loop
  x = aboard->last_row/3 -1;
  for(y=3; y<14; y++) {
    placeItem(aboard,y,x,BC_BARRIER,SYMBOL_BARRIER,COLP_BARRIER);
  }

  x = (aboard->last_row/2) +10;
  for(y=5; y<16; y++) {
      placeItem(aboard,y,x,BC_BARRIER,SYMBOL_BARRIER,COLP_BARRIER);
  }
  //Food
  placeItem(aboard,0,0,BC_FOOD_1,SYMBOL_FOOD_1,COLP_FOOD_1);
  placeItem(aboard,3,4,BC_FOOD_1,SYMBOL_FOOD_1,COLP_FOOD_1);

  placeItem(aboard,6,11,BC_FOOD_2,SYMBOL_FOOD_2,COLP_FOOD_2);
  placeItem(aboard,8,2,BC_FOOD_2,SYMBOL_FOOD_2,COLP_FOOD_2);
  placeItem(aboard,1,16,BC_FOOD_2,SYMBOL_FOOD_2,COLP_FOOD_2);
  placeItem(aboard,0,14,BC_FOOD_2,SYMBOL_FOOD_2,COLP_FOOD_2);

  placeItem(aboard,1,3,BC_FOOD_3,SYMBOL_FOOD_3,COLP_FOOD_3);
  placeItem(aboard,5,3,BC_FOOD_3,SYMBOL_FOOD_3,COLP_FOOD_3);
  placeItem(aboard,3,3,BC_FOOD_3,SYMBOL_FOOD_3,COLP_FOOD_3);
  placeItem(aboard,0,3,BC_FOOD_3,SYMBOL_FOOD_3,COLP_FOOD_3);

  // Initialize number of food items
  // Attention: must match number of items placed on the board above
  aboard->food_items= 10;
  return RES_OK;
}

// Getters

// Get the last usable row on the display
int getLastRowOnBoard(struct board* aboard) {
  return aboard->last_row;
}

// Get the last usable column on the display
int getLastColOnBoard(struct board* aboard) {
  return aboard->last_col;
}

int getNumberOfFoodItems(struct board* aboard) {
  return aboard->food_items;
}

enum BoardCodes getContentAt(struct board* aboard, struct pos position) {
  return aboard->cells[position.y][position.x];
}

// Setters
void setNumberOfFoodItems(struct board* aboard, int n) {
  aboard->food_items = n;
}

void decrementNumberOfFoodItems(struct board* aboard) {
  aboard->food_items--;
}
