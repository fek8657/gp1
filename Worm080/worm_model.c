// A simple variant of the game Snake
//
// Used for teaching in classes
//
// Author:
// Franz Regensburger
// Ingolstadt University of Applied Sciences
// (C) 2011
//
// The worm model
#include <curses.h>
#include "worm.h"
#include "board_model.h"
#include "worm_model.h"

// Initialize the worm
enum ResCodes initializeWorm(struct worm* aworm, int len_max, int len_cur,
    struct pos headpos, enum WormHeading dir, enum ColorPairs color) {
  int i;

  aworm->maxindex = len_max -1 ;
  aworm->cur_lastindex = len_cur -1;
  aworm->headindex = 0; //start on beginning of array

  // Initialize array ignore first pos in array (aworm->headindex)
  for(i=1; i<=aworm->maxindex; i++) {
    aworm->wormpos[i].y = UNUSED_POS_ELEM;
    aworm->wormpos[i].x = UNUSED_POS_ELEM;
  }
  //set head:
  aworm->wormpos[aworm->headindex] = headpos;

  // Initialize the heading of the worm
  setWormHeading(aworm,dir);

  // Initialze color of the worm
  aworm->wcolor = color;

  return RES_OK;
}

void growWorm(struct worm* aworm, enum Boni growth) {
  if(aworm->cur_lastindex + growth <= aworm->maxindex) {
    aworm->cur_lastindex += growth;
  } else {
    aworm->cur_lastindex = aworm->maxindex;
  }
}

// Show the worms's elements on the display
// Simple version
void showWorm(struct board* aboard, struct worm* aworm) {
  int tailindex = ( aworm->headindex + 1 ) % ( aworm->cur_lastindex + 1 );
  int i = aworm->headindex; //initialize i to head position
  char symbol;
  do {
    if(i == aworm->headindex) { //head
      symbol = SYMBOL_WORM_HEAD_ELEMENT;
    } else if(i != tailindex ) { //body
      symbol = SYMBOL_WORM_INNER_ELEMENT;
    } else { //tail
      symbol = SYMBOL_WORM_TAIL_ELEMENT;
    }

    // print worm part
    placeItem(aboard,
              aworm->wormpos[i].y,
              aworm->wormpos[i].x,
              BC_USED_BY_WORM,
              symbol,
              aworm->wcolor);

    i = (i+aworm->cur_lastindex) % (aworm->cur_lastindex + 1);

  } while(aworm->wormpos[i].x != UNUSED_POS_ELEM && i != aworm->headindex);
}


void cleanWormTail(struct board* aboard, struct worm* aworm) {
  int tailindex = (aworm->headindex + 1) % ( aworm->cur_lastindex + 1);
  if(aworm->wormpos[tailindex].y != UNUSED_POS_ELEM) {
    placeItem(aboard,
              aworm->wormpos[tailindex].y,
              aworm->wormpos[tailindex].x,
              BC_FREE_CELL,
              SYMBOL_FREE_CELL,
              COLP_FREE_CELL);
  }
}

void moveWorm(struct board* aboard, struct worm* aworm, enum GameStates* agame_state) {
  struct pos headpos = aworm->wormpos[aworm->headindex];
  headpos.y += aworm->dy;
  headpos.x += aworm->dx;

  if (headpos.x < 0) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (headpos.x > getLastColOnBoard(aboard) ) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (headpos.y < 0) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (headpos.y > getLastRowOnBoard(aboard) ) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else {
    switch(getContentAt(aboard,headpos)) {
    case BC_FOOD_1:
      *agame_state = WORM_GAME_ONGOING;
      growWorm(aworm, BONUS_1);
      decrementNumberOfFoodItems(aboard);
      break;
    case BC_FOOD_2:
      *agame_state = WORM_GAME_ONGOING;
      growWorm(aworm, BONUS_2);
      decrementNumberOfFoodItems(aboard);
      break;
    case BC_FOOD_3:
      *agame_state = WORM_GAME_ONGOING;
      growWorm(aworm, BONUS_3);
      decrementNumberOfFoodItems(aboard);
      break;
    case BC_BARRIER:
      *agame_state = WORM_CRASH;
      break;
    case BC_USED_BY_WORM:
      *agame_state = WORM_CROSSING;
      break;
    default:
      {;}
    }
  }
  if(*agame_state == WORM_GAME_ONGOING) {
    aworm->headindex = (aworm->headindex + 1) % ( aworm->cur_lastindex + 1);
    aworm->wormpos[aworm->headindex] = headpos;
  }
}

//Getters
int getWormLength(struct worm* aworm) {
  return (aworm->cur_lastindex + 1);
}

void setWormHeading(struct worm* aworm, enum WormHeading dir) {
  switch(dir) {
  case WORM_UP:// User wants up
    aworm->dx=0;
    aworm->dy=-1;
    break;
  case WORM_DOWN:// User wants down
    aworm->dx=0;
    aworm->dy=1;
    break;
  case WORM_LEFT:// User wants left
    aworm->dx=-1;
    aworm->dy=0;
    break;
  case WORM_RIGHT:// User wants right
    aworm->dx=1;
    aworm->dy=0;
    break;
  }
}

struct pos getWormHeadPos(struct worm* aworm) {
  return aworm->wormpos[aworm->headindex];
}
